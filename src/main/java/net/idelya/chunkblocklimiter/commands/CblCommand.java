package net.idelya.chunkblocklimiter.commands;

import net.idelya.chunkblocklimiter.Main;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class CblCommand extends BukkitCommand {
    private final Main main;

    public CblCommand(Main main) {
        super("cbl");
        this.main = main;
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (!sender.hasPermission("cbl.reload")) {
            sender.sendMessage("§cTu n'as pas de la permission d'exécuter cette commande");
            return true;
        }
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("reload")) {
                main.reloadConfig();
                main.loadCheckedMaterials();
                main.loadOtherConfig();
                sender.sendMessage("[CBL] Reloaded");
            }
        }
        return true;
    }
}
