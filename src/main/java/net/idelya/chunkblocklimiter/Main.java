package net.idelya.chunkblocklimiter;

import net.idelya.chunkblocklimiter.commands.CblCommand;
import net.idelya.chunkblocklimiter.listeners.OnBlockPlace;
import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Main extends JavaPlugin {
    private final HashMap<Material, Integer> checkedMaterials = new HashMap<>();
    private String errorMessage;
    private boolean ignoreCreative;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        if (!this.getConfig().contains("materials")) {
            this.getConfig().set("materials", Collections.singletonList("FURNACE;10"));
            this.saveConfig();
        }

        if (!this.getConfig().contains("error_message")) {
            this.getConfig().set("error_message", "&4[!] &cVous ne pouvez pas mettre plus de %number% %item_name%.");
            this.saveConfig();
        }

        if (!this.getConfig().contains("ignore_creative")) {
            this.getConfig().set("ignore_creative", true);
            this.saveConfig();
        }

        this.getServer().getPluginManager().registerEvents(new OnBlockPlace(this), this);
        this.getServer().getCommandMap().register("cbl", new CblCommand(this));

        loadCheckedMaterials();
        loadOtherConfig();
    }

    public HashMap<Material, Integer> checkedMaterials() {
        return checkedMaterials;
    }

    public void loadCheckedMaterials() {
        List<String> materialsAsString = this.getConfig().getStringList("materials");
        for (String materialAsString : materialsAsString) {
            try {
                String[] split = materialAsString.split(";");
                if (split.length != 2) continue;
                checkedMaterials.put(Material.valueOf(split[0].toUpperCase()), Integer.parseInt(split[1]));
            } catch (IllegalArgumentException ignored) {
            }
        }
    }

    public void loadOtherConfig() {
        errorMessage = this.getConfig().getString("error_message");
        ignoreCreative = this.getConfig().getBoolean("ignore_creative");
    }

    public String errorMessage() {
        return errorMessage;
    }

    public boolean isIgnoreCreative() {
        return ignoreCreative;
    }
}
