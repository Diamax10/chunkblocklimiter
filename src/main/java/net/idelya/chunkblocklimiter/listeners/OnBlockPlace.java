package net.idelya.chunkblocklimiter.listeners;

import net.idelya.chunkblocklimiter.Main;
import org.bukkit.Chunk;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class OnBlockPlace implements Listener {
    private final Main main;

    public OnBlockPlace(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (main.isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) return;

        if (main.checkedMaterials().containsKey(event.getBlock().getType())) {
            int counter = 0;
            Chunk chunk = event.getBlock().getChunk();
            for (int x = 0; x < 16; ++x) {
                int xCoord = chunk.getX() * 16 + x;
                for (int z = 0; z < 16; ++z) {
                    int zCoord = chunk.getZ() * 16 + z;
                    for (int y = 0; y < 256; ++y) {
                        Location location = new Location(event.getBlock().getChunk().getWorld(), xCoord, y, zCoord);
                        if (location.getBlock().getType().equals(event.getBlock().getType())) {
                            counter++;
                        }
                    }
                }
            }
            if (main.checkedMaterials().get(event.getBlock().getType()) < counter) {
                event.setCancelled(true);
                String errorMessage = this.main.errorMessage()
                        .replace("&", "§")
                        .replace("%number%", String.format("%d", main.checkedMaterials().get(event.getBlock().getType())))
                        .replace("%item_name%", event.getBlock().getType().name()
                                .replace("_", " ")
                                .toLowerCase()
                        );
                event.getPlayer().sendMessage(errorMessage);
            }
        }
    }
}
